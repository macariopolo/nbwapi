package edu.uclm.esi.nbwapi.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.nbwapi.model.ClassicMutant;

@Repository
public interface ClassicMutantDAO extends CrudRepository<ClassicMutant, String> {

	@Query(value = "update test_method set execution_time= :time where id=:testMethodId", nativeQuery = true)
	void updateTime(@Param("testMethodId") long testMethodId, @Param("time") long time);

}
