package edu.uclm.esi.nbwapi.model;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Optional;

import javax.persistence.Entity;

import edu.uclm.esi.nbwapi.web.controllers.MavenAPIManager;

@Entity
public class MavenProjectConfiguration extends ProjectConfiguration {

	public MavenProjectConfiguration() {
		super();
	}
	
	@Override
	protected void downloadMutant(Project project, String mutantId, Object dado) throws Exception {
		Optional<ClassicMutant> optMutant = MavenAPIManager.get().getClassicMutantDAO().findById(mutantId);
		if (optMutant.isPresent()) {
			ClassicMutant mutant = optMutant.get();
			ProjectFile projectFile = mutant.getOriginalFile();
			String fileName = this.getProjectPath(project, dado) + projectFile.getFileName();
			try(FileOutputStream fos = new FileOutputStream(fileName)) {
				fos.write(mutant.getCode().getBytes());
			}
		}
	}

	@Override
	protected String getProjectPath(Project project, Object dado) {
		return MavenAPIManager.get().getReceivedMavenProjects() + dado + File.separatorChar + project.getName() + File.separatorChar;
	}
}
