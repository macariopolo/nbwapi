package edu.uclm.esi.nbwapi.model;

import org.json.JSONObject;

import edu.uclm.esi.nbwapi.so.ProcessExecutor;

public class Device {

	private String name;

	public Device(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public JSONObject push(Project project, boolean testAPK, Object dado) throws Exception {
		AndroidProjectConfiguration configuration = ((AndroidProjectConfiguration) project.getProjectConfiguration());
		String pushCommand;
		
		if (!testAPK) {
			pushCommand= Manager.get().getSdkPath() +  "platform-tools/adb -s #DEVICE# push " + project.getProjectPath(dado) + 
					configuration.getDebugAPKPathAndName() + " " + configuration.getDebugPackage();
		} else {
			pushCommand= Manager.get().getSdkPath() +  "platform-tools/adb -s #DEVICE# push " + project.getProjectPath(dado) + 
					configuration.getTestAPKPathAndName() + " " + configuration.getTestPackage();
		}
		
		pushCommand = pushCommand.replace("#DEVICE#", this.getName());
		ProcessExecutor processExecutor = new ProcessExecutor(project.getProjectPath(dado), pushCommand);
		processExecutor.start();
		JSONObject executionResult = processExecutor.getExecutionResult();
		executionResult.put("type", testAPK ? "testAPKPushed" : "APKPushed");
		return executionResult;
	}
	
	public JSONObject install(Project project, boolean testAPK, Object dado) throws Exception {
		AndroidProjectConfiguration configuration = ((AndroidProjectConfiguration) project.getProjectConfiguration());
		String command = Manager.get().getSdkPath() + "platform-tools/adb -s #DEVICE# shell pm install -t -r ";
		command = command.replace("#DEVICE#", this.getName());
		if (testAPK) {
			command =  command + configuration.getTestPackage();
		} else {
			command = command + configuration.getDebugPackage();
		}
		ProcessExecutor processExecutor = new ProcessExecutor(project.getProjectPath(dado), command);
		processExecutor.start();
		JSONObject executionResult = processExecutor.getExecutionResult();
		executionResult.put("type", testAPK ? "testAPKInstalled" : "APKInstalled");
		return executionResult;
	}

	public JSONObject runTestMethod(String sdkPath, String testClassName, String testMethodName, String testRunner) {
		String command = sdkPath + "platform-tools/adb -s " + this.name + 
				" shell am instrument -w -r -e debug false -e class '" + testClassName + "#" + testMethodName + "' " + 
				testRunner;
		
		ProcessExecutor processExecutor = new ProcessExecutor(Manager.get().getIo(), command);
		processExecutor.start();
		JSONObject deviceResponse = processExecutor.getExecutionResult();
		return deviceResponse;
	}
}
