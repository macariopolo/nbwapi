package edu.uclm.esi.nbwapi.web.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.nbwapi.model.Device;

@RestController
@RequestMapping("android")
public class AndroidController {
	
	@GetMapping(value = "/loadEmulators", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public List<String> loadEmulators() throws Exception {
		return AndroidAPIManager.loadEmulators();
	}
	
	@GetMapping(value = "/loadDevices", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public List<Device> loadDevices() throws Exception {
		return AndroidAPIManager.loadDevices();
	}
	
	@GetMapping(value = "/downloadProject", produces = MediaType.APPLICATION_JSON_VALUE)
	public String downloadProject(HttpSession session, @RequestParam String projectId) throws FileNotFoundException, IOException {
		session.setMaxInactiveInterval(1800);
		session.setAttribute("dado", new SecureRandom().nextInt(10000));
		System.out.println("downloadProject " + projectId);
		return AndroidAPIManager.downloadProject(projectId, session.getAttribute("dado"));
	}
	
	@GetMapping(value = "/compile", produces = MediaType.APPLICATION_JSON_VALUE)
	public String compile(HttpSession session, @RequestParam String projectId) {
		session.setMaxInactiveInterval(1800);
		System.out.println("compile " + projectId);
		return AndroidAPIManager.compile(projectId, null, session.getAttribute("dado")).toString();
	}
	
	@GetMapping(value = "/compileTest", produces = MediaType.APPLICATION_JSON_VALUE)
	public String compileTest(HttpSession session, @RequestParam String projectId) {
		session.setMaxInactiveInterval(1800);
		System.out.println("compileTest " + projectId);
		return AndroidAPIManager.compileTest(projectId, session.getAttribute("dado"));
	}
	
	@PostMapping(value = "/push", produces = MediaType.APPLICATION_JSON_VALUE)
	public String push(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		String deviceName = map.get("deviceName").toString();
		String projectId = map.get("projectId").toString();
		boolean testAPK = (boolean) map.get("testAPK");		
		System.out.println("push " + deviceName + " " + projectId + " " + testAPK);
		return AndroidAPIManager.push(deviceName, projectId, testAPK, session.getAttribute("dado")).toString();
	}
	
	@PostMapping(value = "/install", produces = MediaType.APPLICATION_JSON_VALUE)
	public String install(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		session.setMaxInactiveInterval(1800);
		String deviceName = map.get("deviceName").toString();
		String projectId = map.get("projectId").toString();
		boolean testAPK = (boolean) map.get("testAPK");
		System.out.println("install " + deviceName + " " + projectId + " " + testAPK);
		return AndroidAPIManager.install(deviceName, projectId, testAPK, session.getAttribute("dado")).toString();
	}
	
	@PostMapping(value = "/downloadAndInstallNextMutant", produces = MediaType.APPLICATION_JSON_VALUE)
	public String downloadAndInstallNextMutant(HttpSession session, @RequestBody Map<String, Object> map) throws Exception {
		session.setMaxInactiveInterval(1800);
		String projectId = map.get("projectId").toString();
		@SuppressWarnings("unchecked")
		Map<String, Object> mapMW = (Map<String, Object>) map.get("mutantWrapper");
		JSONObject mutantWrapper = new JSONObject(mapMW);
		String deviceName = map.get("deviceName").toString();
		System.out.println("downloadAndInstallNextMutant " + deviceName + " " + projectId + " " + mutantWrapper);
		JSONObject response = AndroidAPIManager.downloadMutant(projectId, mutantWrapper, session.getAttribute("dado"));
		if (response.getInt("returnCode")!=0) 
			return response.put("type", "mutantInstalled").put("mutantWrapper", mutantWrapper).toString();
		response = AndroidAPIManager.compile(projectId, mutantWrapper.optString("id"), session.getAttribute("dado"));
		if (response.getInt("returnCode")!=0) 
			return response.put("type", "mutantInstalled").put("mutantWrapper", mutantWrapper).toString();
		response = AndroidAPIManager.push(deviceName, projectId, false, session.getAttribute("dado"));
		if (response.getInt("returnCode")!=0) 
			return response.put("type", "mutantInstalled").put("mutantWrapper", mutantWrapper).toString();
		response = AndroidAPIManager.install(deviceName, projectId, false, session.getAttribute("dado"));
		return response.put("type", "mutantInstalled").put("mutantWrapper", mutantWrapper).toString();
	}
	
	@PostMapping(value = "/runTestMethod", produces = MediaType.APPLICATION_JSON_VALUE)
	public String runTestMethod(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		String projectId = map.get("projectId").toString();
		String deviceName = map.get("deviceName").toString();
		String testClassName = map.get("testClassName").toString();
		String testMethodName = map.get("testMethodName").toString();
		JSONObject mutantWrapper = null;
		if (map.containsKey("mutantWrapper")) {
			@SuppressWarnings("unchecked")
			Map<String, Object> mapMW = (Map<String, Object>) map.get("mutantWrapper");
			mutantWrapper = new JSONObject(mapMW);
		}
		System.out.println("runTestMethod " + deviceName + " " + projectId + " " + testClassName + "#" + testMethodName + " on " +
				(mutantWrapper!=null ? mutantWrapper : "original"));
		return AndroidAPIManager.runTestMethod(projectId, deviceName, testClassName, testMethodName, mutantWrapper);
	}
	
	@PostMapping(value = "/stopTests", produces = MediaType.APPLICATION_JSON_VALUE)
	public String stopTests(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		String projectId = map.get("projectId").toString();
		String deviceName = map.get("deviceName").toString();
		String packageName = map.get("packageName").toString();
		System.out.println("stopTests " +projectId + " " +  deviceName + " " +  packageName);
		return AndroidAPIManager.stopTests(projectId, deviceName, packageName);
	}
}
