package edu.uclm.esi.nbwapi.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONObject;

import edu.uclm.esi.nbwapi.so.ProcessExecutor;

public class APKCompiler {
	
	public synchronized static JSONObject compile(Project project, String mutantId, Object dado) throws FileNotFoundException, IOException {
		String projectFolder = Manager.get().getReceivedAPKs() + project.getId() + File.separatorChar + project.getName() + File.separatorChar;
		String buildAPKCommand = ((AndroidProjectConfiguration) project.getProjectConfiguration()).getBuildAPKCommand();
		JSONObject executionResult = compile(projectFolder, buildAPKCommand);
		if (executionResult.getInt("returnCode")!=0 && mutantId!=null) {
			ClassicMutant mutant = Manager.get().getClassicMutantDAO().findById(mutantId).get();
			ProjectFile originalFile = mutant.getOriginalFile();
			String fileName = project.getProjectPath(dado) + originalFile.getFileName();
			try(FileOutputStream fos = new FileOutputStream(fileName)) {
				fos.write(originalFile.getContents());
			}
		}
		return executionResult;
	}
	
	public static JSONObject compileTest(Project project, Object dado) {
		String projectFolder = Manager.get().getReceivedAPKs() + dado + File.separatorChar + project.getName() + File.separatorChar;
		String buildTestAPKCommand = ((AndroidProjectConfiguration) project.getProjectConfiguration()).getBuildTestAPKCommand();
		return compileTest(projectFolder, buildTestAPKCommand);
	}

	private static JSONObject compile(String projectFolder, String buildAPKCommand) {
		String osName = System.getProperty("os.name");
		
		JSONObject jso;
		if (!osName.startsWith("Windows")) {
			String chmod="chmod +x " + projectFolder + "gradlew";
			ProcessExecutor processExecutor = new ProcessExecutor(projectFolder, chmod);
			processExecutor.start();
			jso = processExecutor.getExecutionResult();
			if (jso.getInt("returnCode")!=0)
				return jso;
		} else {
			buildAPKCommand = buildAPKCommand.replace("./gradlew", "gradlew.bat");
			buildAPKCommand = projectFolder + buildAPKCommand ;
		}
		ProcessExecutor processExecutor = new ProcessExecutor(projectFolder, buildAPKCommand);
		processExecutor.start();
		jso = processExecutor.getExecutionResult();
		return jso;
	}

	private static JSONObject compileTest(String projectFolder, String buildTestAPKCommand) {
		String osName = System.getProperty("os.name");
		
		JSONObject jso;
		if (!osName.startsWith("Windows")) {
			String chmod="chmod +x " + projectFolder + "gradlew";
			ProcessExecutor processExecutor = new ProcessExecutor(projectFolder, chmod);
			processExecutor.start();
			jso = processExecutor.getExecutionResult();
			if (jso.getInt("returnCode")!=0)
				return jso;
		} else {
			buildTestAPKCommand = buildTestAPKCommand.replace("./gradlew", "gradlew.bat");
			buildTestAPKCommand = projectFolder + buildTestAPKCommand ;
		}
		ProcessExecutor processExecutor = new ProcessExecutor(projectFolder, buildTestAPKCommand);
		processExecutor.start();
		jso = processExecutor.getExecutionResult();
		return jso;
	}
}
