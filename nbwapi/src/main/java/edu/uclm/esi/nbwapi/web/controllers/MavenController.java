package edu.uclm.esi.nbwapi.web.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.nbwapi.model.Project;

@RestController
@RequestMapping("maven")
public class MavenController {
	
	private String hostId;

	@GetMapping(value = "/echo", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public void echo(HttpSession session, @RequestParam String hostId)  {
		this.hostId = hostId;
		System.out.println("echo: " + hostId);
	}
	
	@PostMapping(value = "/downloadProject", produces = MediaType.APPLICATION_JSON_VALUE)
	public String downloadProject(HttpSession session, @RequestBody Map<String, Object> info) throws FileNotFoundException, IOException {
		session.setMaxInactiveInterval(1800);
		System.out.println("downloadProject: " + new JSONObject(info) + "; hostId: " + this.hostId);
		return MavenAPIManager.get().downloadProject(info.get("projectId").toString(), info.get("mavenHost").toString(), this.hostId);
	}
	
	@PostMapping(value = "/downloadAndInstallNextMutant", produces = MediaType.APPLICATION_JSON_VALUE)
	public String downloadAndInstallNextMutant(HttpSession session, @RequestBody Map<String, Object> map) throws Exception {
		session.setMaxInactiveInterval(1800);
		JSONObject jsoInfo = new JSONObject(map);
		
		String projectId = jsoInfo.getString("projectId");
		Optional<Project> optProject = MavenAPIManager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			JSONObject jsoCurrentMutant = jsoInfo.optJSONObject("previousMutant");
			if (jsoCurrentMutant!=null) {
				String projectFileId = jsoCurrentMutant.getString("classUnderTest");
				project.downloadOriginalFile(projectFileId, this.hostId);
			}
			
			JSONObject mutantWrapper = jsoInfo.getJSONObject("mutantWrapper");
			System.out.println("downloadAndInstallNextMutant " + projectId + " " + mutantWrapper);
			JSONObject response = MavenAPIManager.get().downloadMutant(project, mutantWrapper, this.hostId);
			return response.put("type", "mutantInstalled").put("mutantWrapper", mutantWrapper).toString();
		} else {
			return new JSONObject().put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found in remote host")).toString();
		}
	}
	
	@PostMapping(value = "/runTestMethod", produces = MediaType.APPLICATION_JSON_VALUE)
	public String runTestMethod(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		JSONObject jso = new JSONObject(map);
		String projectId = jso.getString("projectId");
		String mavenPath = jso.getString("mavenPath");
		String desiredPath = jso.getString("desiredPath");
		String javaHome = jso.getString("javaHome");
		String testClassName = jso.getString("testClassName");
		String testMethodName = jso.getString("testMethodName");
		long testMethodId = jso.getLong("testMethodId");
		JSONObject mutantWrapper = jso.optJSONObject("mutantWrapper");

		System.out.println("runTestMethod " + projectId + " " + testClassName + "#" + testMethodName + " on " +
				(mutantWrapper!=null ? mutantWrapper : "original"));
		if (mutantWrapper!=null) {
			System.out.println("Mutant: " + mutantWrapper.getInt("index") + "-> " + testMethodName);
			testMethodId = -1;
		}
		String result = MavenAPIManager.get().runTestMethod(projectId, mavenPath, desiredPath, javaHome, testClassName, testMethodName, mutantWrapper, testMethodId, this.hostId);
		return result;
	}
	
	@PostMapping(value = "/stopTests", produces = MediaType.APPLICATION_JSON_VALUE)
	public String stopTests(HttpSession session,  @RequestBody Map<String, Object> map) throws Exception {
		String projectId = map.get("projectId").toString();
		String packageName = map.get("packageName").toString();
		System.out.println("stopTests " +projectId + " " +  packageName);
		return MavenAPIManager.get().stopTests(projectId, packageName);
	}
}
