package edu.uclm.esi.nbwapi.model;

import java.io.File;

import org.json.JSONObject;

import edu.uclm.esi.nbwapi.so.ProcessExecutor;
import edu.uclm.esi.nbwapi.web.controllers.MavenAPIManager;

public class MavenCompilerAndRunner {
	
	private static JSONObject execute(String projectFolder, String command, String desiredPath, String javaHome) {
		ProcessExecutor processExecutor = new ProcessExecutor(projectFolder, command, desiredPath, javaHome);
		processExecutor.start();
		return processExecutor.getExecutionResult();
	}

	public static JSONObject runTestMethod(Project project, String mavenPath, String desiredPath, String javaHome, String testClassName, String testMethodName, long testMethodId, Object dado) {
		// mvn -Dtest=UsersServiceImpl#testCreateUser test
		String projectFolder = MavenAPIManager.get().getReceivedMavenProjects() + dado + File.separatorChar + project.getName() + File.separatorChar;
		String command = mavenPath + " -q -Dtest=" + testClassName + "#" + testMethodName + " test";
		//command = "ls";
		long time = System.currentTimeMillis();
		JSONObject jso = execute(projectFolder, command, desiredPath, javaHome);
		time = System.currentTimeMillis()-time;
		if (testMethodId!=-1) {
			MavenAPIManager.get().getClassicMutantDAO().updateTime(testMethodId, time);
		}
		return jso;
	}
}
