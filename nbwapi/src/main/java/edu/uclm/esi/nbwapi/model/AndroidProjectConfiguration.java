package edu.uclm.esi.nbwapi.model;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Optional;

import javax.persistence.Entity;

@Entity
public class AndroidProjectConfiguration extends ProjectConfiguration {
	private String gradlewCommand;
	private String localProperties;
	private String appFolder;
	private String buildGradle;
	private String manifestPath;
	private String applicationId;
	private String testRunner;	
	private String buildAPKCommand;
	private String installAPKCommand;
	private String debugAPKPathAndName;
	private String debugPackage;
	private String buildTestAPKCommand;
	private String installTestAPKCommand;
	private String testAPKPathAndName;
	private String testPackage;
	private String unitTestCommand;
	private String compileSdkVersion;
	
	public AndroidProjectConfiguration() {
		super();
	}
	
	public String getGradlewCommand() {
		return gradlewCommand;
	}

	public void setGradlewCommand(String gradlewCommand) {
		this.gradlewCommand = gradlewCommand;
	}
	
	public String getLocalProperties() {
		return localProperties;
	}

	public void setLocalProperties(String localProperties) {
		this.localProperties = localProperties;
	}

	public String getAppFolder() {
		return appFolder;
	}

	public void setAppFolder(String appFolder) {
		this.appFolder = appFolder;
	}

	public String getBuildGradle() {
		return buildGradle;
	}

	public void setBuildGradle(String buildGradle) {
		this.buildGradle = buildGradle;
	}

	public String getManifestPath() {
		return manifestPath;
	}

	public void setManifestPath(String manifestPath) {
		this.manifestPath = manifestPath;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTestRunner() {
		return testRunner;
	}

	public void setTestRunner(String testRunner) {
		this.testRunner = testRunner;
	}

	public String getBuildAPKCommand() {
		return buildAPKCommand;
	}

	public void setBuildAPKCommand(String buildAPKCommand) {
		this.buildAPKCommand = buildAPKCommand;
	}

	public String getInstallAPKCommand() {
		return installAPKCommand;
	}

	public void setInstallAPKCommand(String installAPKCommand) {
		this.installAPKCommand = installAPKCommand;
	}

	public String getDebugAPKPathAndName() {
		return debugAPKPathAndName;
	}

	public void setDebugAPKPathAndName(String debugAPKPathAndName) {
		this.debugAPKPathAndName = debugAPKPathAndName;
	}

	public String getDebugPackage() {
		return debugPackage;
	}

	public void setDebugPackage(String debugPackage) {
		this.debugPackage = debugPackage;
	}

	public String getBuildTestAPKCommand() {
		return buildTestAPKCommand;
	}

	public void setBuildTestAPKCommand(String buildTestAPKCommand) {
		this.buildTestAPKCommand = buildTestAPKCommand;
	}

	public String getInstallTestAPKCommand() {
		return installTestAPKCommand;
	}

	public void setInstallTestAPKCommand(String installTestAPKCommand) {
		this.installTestAPKCommand = installTestAPKCommand;
	}

	public String getTestAPKPathAndName() {
		return testAPKPathAndName;
	}
	
	public void setTestAPKPathAndName(String testAPKPathAndName) {
		this.testAPKPathAndName = testAPKPathAndName;
	}
	
	public String getTestPackage() {
		return testPackage;
	}

	public void setTestPackage(String testPackage) {
		this.testPackage = testPackage;
	}

	public String getUnitTestCommand() {
		return unitTestCommand;
	}

	public void setUnitTestCommand(String unitTestCommand) {
		this.unitTestCommand = unitTestCommand;
	}

	public void setCompileSdkVersion(String compileSdkVersion) {
		this.compileSdkVersion = compileSdkVersion;
	}
	
	public String getCompileSdkVersion() {
		return compileSdkVersion;
	}

	@Override
	protected void downloadMutant(Project project, String mutantId, Object dado) throws Exception {
		Optional<ClassicMutant> optMutant = Manager.get().getClassicMutantDAO().findById(mutantId);
		if (optMutant.isPresent()) {
			ClassicMutant mutant = optMutant.get();
			ProjectFile projectFile = mutant.getOriginalFile();
			String fileName = this.getProjectPath(project, dado) + projectFile.getFileName();
			try(FileOutputStream fos = new FileOutputStream(fileName)) {
				fos.write(mutant.getCode().getBytes());
			}
		}
	}
	
	@Override
	protected String getProjectPath(Project project, Object dado) {
		return Manager.get().getReceivedAPKs() + project.getId() + File.separatorChar + project.getName() + File.separatorChar;
	}
}
