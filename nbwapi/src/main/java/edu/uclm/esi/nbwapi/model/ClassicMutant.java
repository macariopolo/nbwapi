package edu.uclm.esi.nbwapi.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ClassicMutant {
	@Id
	private String id;
	@ManyToOne
	@JsonIgnore
	private ProjectFile originalFile;
	@Column(columnDefinition = "LONGTEXT")
	@Lob
	@JsonIgnore
	private String code;
	@Column
	private int mutantIndex;
	@Column
	private String operator;
	
	public ClassicMutant() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ProjectFile getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(ProjectFile originalFile) {
		this.originalFile = originalFile;
	}

	@JsonIgnore
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setIndex(int mutantIndex) {
		this.mutantIndex = mutantIndex;
	}

	public int getMutantIndex() {
		return mutantIndex;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public String getOperator() {
		return operator;
	}
}
