package edu.uclm.esi.nbwapi.model;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import edu.uclm.esi.nbwapi.web.controllers.MavenAPIManager;

@Entity
public abstract class ProjectConfiguration {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String srcFolder;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> libraries;
	
	public ProjectConfiguration() {
		this.libraries = new ArrayList<>();
	}
	
	public void addLibrary(String library) {
		this.libraries.add(library);
	}
	
	public void removeLibrary(String library) {
		this.libraries.remove(library);
	}
	
	public void setLibraries(List<String> libraries) {
		this.libraries = libraries;
	}
	
	public List<String> getLibraries() {
		return libraries;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSrcFolder() {
		return srcFolder;
	}

	public void setSrcFolder(String srcFolder) {
		this.srcFolder = srcFolder;
	}

	protected abstract void downloadMutant(Project project, String mutantId, Object dado) throws Exception;
	
	protected abstract String getProjectPath(Project project, Object dado);

	protected void downloadOriginalFile(Project project, String mutantId, Object dado) throws Exception {
		Optional<ProjectFile> optOriginalFile = MavenAPIManager.get().getProjectFileDAO().findById(mutantId);
		if (optOriginalFile.isPresent()) {
			ProjectFile projectFile = optOriginalFile.get();
			String fileName = this.getProjectPath(project, dado) + projectFile.getFileName();
			try(FileOutputStream fos = new FileOutputStream(fileName)) {
				fos.write(projectFile.getContents());
			}
		}
	}
}
