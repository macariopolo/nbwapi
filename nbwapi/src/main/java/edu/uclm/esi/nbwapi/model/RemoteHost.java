package edu.uclm.esi.nbwapi.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.nbwapi.so.ProcessExecutor;

@Entity
public class RemoteHost {
	@Id 
	private String ip;
	private Integer port;
	private String sdkPath;
	private String avdPath;
	@Transient
	private List<Device> devices;
	
	public RemoteHost() {
		this.devices = new Vector<>();
	}
	
	public RemoteHost(String ipAndPort) {
		this();
		int posDosPuntos = ipAndPort.indexOf(':');
		this.ip = ipAndPort.substring(0, posDosPuntos);
		this.port = Integer.parseInt(ipAndPort.substring(posDosPuntos+1));
	}
	
	public List<String> loadEmulators() {
		String avdPath = this.avdPath;
		ArrayList<String> emulators = new ArrayList<>();
		File folder=new File(avdPath);
		File[] listOfFiles = folder.listFiles();
		String pathName, deviceName;
		for (int i = 0; i < listOfFiles.length; i++) {
	    	if (listOfFiles[i].isDirectory()) {
	    		pathName=listOfFiles[i].getName();
		    	if (listOfFiles[i].isDirectory() && pathName.endsWith("avd")) {
		    		deviceName=pathName.substring(0, pathName.length()-4);
		    		emulators.add(deviceName);
		    	}
	    	}
	    }
	    return emulators;
	}
	public List<Device> loadDevices() {
		this.devices.clear();
		String sdkPath = this.sdkPath;
		String command = sdkPath + "platform-tools/adb devices -l";
		ProcessExecutor processExecutor = new ProcessExecutor(command);
		processExecutor.start();
		JSONObject jso = processExecutor.getExecutionResult();
		JSONArray outputLines = jso.getJSONArray("outputLines");
		String line, name;
		Device device;
		for (int i=1; i<outputLines.length(); i++) {
			line = outputLines.getString(i);
			if (line.length()>0) {
				line = line.substring(0, line.indexOf(' '));
				name = line.trim();
				device = findDevice(name);
				if (device==null)
					this.devices.add(new Device(name));
			}
		}
		return this.devices;
	}
	
	public Device findDevice(String name) {
		for (Device device : this.devices)
			if (device.getName().equals(name))
				return device;
		return null;
	}
	
	public void launchEmulator(String emulator) {
		String sdkPath = this.sdkPath;
		String emulatorCommandPath= sdkPath + "emulator/emulator ";
		StringBuilder sb = new StringBuilder();
		sb.append(emulatorCommandPath);
		sb.append("@" + emulator);
		sb.append(" -netdelay");
		sb.append(" none");
		sb.append(" -netspeed");
		sb.append(" full");
		sb.append(" -no-snapshot-load");
		launch(sb.toString());
	}

	public List<String> launchEmulators(List<String> emulators) {
		ArrayList<String> commands = new ArrayList<>();
		String sdkPath = this.sdkPath;
		String emulatorCommandPath= sdkPath + "emulator/emulator ";
		for (int i=0; i<emulators.size(); i++) {
			StringBuilder sb = new StringBuilder();
			String emulator=emulators.get(i);
			sb.append(emulatorCommandPath);
			sb.append("@" + emulator);
			sb.append(" -netdelay");
			sb.append(" none");
			sb.append(" -netspeed");
			sb.append(" full");
			sb.append(" -no-snapshot-load");
			launch(sb.toString());
			commands.add(sb.toString());
		}
		return commands;
	}

	private void launch(String command) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				ProcessExecutor processExecutor = new ProcessExecutor(command);
				processExecutor.start();
			}
		};
		Thread thread = new Thread(runnable);
		thread.start();
	}

	public List<String> stopEmulators(List<String> emulators) {
		ArrayList<String> commands = new ArrayList<>();
		String sdkPath = this.sdkPath;
		String command = sdkPath + "platform-tools/adb -s";
		for (int i=0; i<emulators.size(); i++) {
			StringBuilder sb = new StringBuilder();
			String emulator=emulators.get(i);
			sb.append(command);
			sb.append(emulator);
			sb.append(" emu");
			sb.append(" kill");
			launch(sb.toString());
			commands.add(sb.toString());
		}
		return commands;
		//-s emulator-5554 emu kill
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Integer getPort() {
		return port;
	}
	
	public void setPort(Integer port) {
		this.port = port;
	}
	
	public String getSdkPath() {
		return sdkPath;
	}

	public void setSdkPath(String sdkPath) {
		if (!sdkPath.endsWith(File.separator))
			sdkPath = sdkPath + File.separatorChar;
		this.sdkPath = sdkPath;
	}

	public String getAvdPath() {
		return avdPath;
	}

	public void setAvdPath(String avdPath) {
		if (!avdPath.endsWith(File.separator))
			avdPath = avdPath + File.separatorChar;
		this.avdPath = avdPath;
	}
}
