package edu.uclm.esi.nbwapi.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.nbwapi.model.RemoteHost;

@Repository
public interface RemoteHostDAO extends CrudRepository<RemoteHost, String> {
}
