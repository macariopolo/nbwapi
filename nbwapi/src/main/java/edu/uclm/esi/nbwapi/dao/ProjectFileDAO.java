package edu.uclm.esi.nbwapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.nbwapi.model.ProjectFile;


@Repository
public interface ProjectFileDAO extends CrudRepository<ProjectFile, String>{

	@Query(value = "SELECT id FROM project_file where project_id=:projectId", nativeQuery = true)
	List<String> findIdsByProject(@Param("projectId") String projectId);
	
	@Query(value = "SELECT id FROM project_file where project_id=:projectId and right(file_name, 5)='.java'", nativeQuery = true)
	List<String> findJavaIdsByProject(@Param("projectId") String projectId);
}
