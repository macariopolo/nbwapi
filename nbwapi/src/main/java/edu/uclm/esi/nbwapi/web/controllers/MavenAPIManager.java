package edu.uclm.esi.nbwapi.web.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import edu.uclm.esi.nbwapi.dao.ClassicMutantDAO;
import edu.uclm.esi.nbwapi.dao.ProjectDAO;
import edu.uclm.esi.nbwapi.dao.ProjectFileDAO;
import edu.uclm.esi.nbwapi.model.MavenCompilerAndRunner;
import edu.uclm.esi.nbwapi.model.Project;
import edu.uclm.esi.nbwapi.model.ProjectFile;

@Component
public class MavenAPIManager {	
	@Autowired
	private ProjectFileDAO projectFileDAO;
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ClassicMutantDAO classicMutantDAO;

	private String root;
	private String io;
	private String receivedMavenProjects;
	
	private MavenAPIManager() {
		createFolders();
	}
	
	private void createFolders() {
		String homeFolder=System.getProperty("user.home");
		this.root = homeFolder + File.separatorChar + "nbwapi" + File.separatorChar;
		this.io = this.root + File.separatorChar + "io" + File.separatorChar;
		this.receivedMavenProjects = this.root + "receivedMavenProjects" + File.separatorChar;
		new File(this.root).mkdir();
		new File(this.io).mkdir();
		new File(this.receivedMavenProjects).mkdir();
	}
	
	public String getIoPath() {
		return System.getProperty("user.home") + File.separatorChar + "nbwapi" + File.separatorChar + "io" + File.separatorChar;
	}
	
	public String getReceivedMavenProjects() {
		return System.getProperty("user.home") + File.separatorChar + "nbwapi" + File.separatorChar + "receivedMavenProjects" + File.separatorChar;
	}
	
	private static class ManagerHolder {
		static MavenAPIManager singleton=new MavenAPIManager();
	}
	
	@Bean
	public static MavenAPIManager get() {
		return ManagerHolder.singleton;
	}
	
	public String downloadProject(String projectId, String mavenHost, Object dado) throws FileNotFoundException, IOException {
		JSONObject response = new JSONObject();
		response.put("type", "projectDownloaded").put("mavenHost", mavenHost);
		
		Project project = this.projectDAO.findById(projectId).get();

		String path = project.getProjectPath(dado);
		try { FileUtils.forceDelete(new File(path)); } catch (Exception e) { }

		File file = new File(path);
		file.mkdirs();

		List<String> files = this.projectFileDAO.findIdsByProject(projectId);
		String projectFileId;
		ProjectFile projectFile;
		String filePath;
		int posLastBarra;
		int numberOfFiles = files.size();
		int contador = 0;
		while (files.size()>0) {
			contador++;
			if (contador % 100 ==0) {
				System.out.println(contador + "/" + numberOfFiles);
			}
			projectFileId = files.remove(0);
			try {
				projectFile = this.projectFileDAO.findById(projectFileId).get();
				filePath = projectFile.getFileName();
				posLastBarra = filePath.lastIndexOf('/');
				if (posLastBarra!=-1) {
					filePath = path + filePath.substring(0, filePath.lastIndexOf('/'));
					new File(filePath).mkdirs();
				}
				filePath = path + projectFile.getFileName();
				try(FileOutputStream fos = new FileOutputStream(filePath)) {
					fos.write(projectFile.getContents());
				}
			} catch (Error e) {
				e.printStackTrace();
			}
		}
		
		return response.toString();
	}
	
	public String runTestMethod(String projectId, String mavenPath, String desiredPath, String javaHome, String testClassName, String testMethodName, JSONObject mutantWrapper, long testMethodId, Object dado) throws Exception {
		JSONObject response = new JSONObject();
		response.put("type", "testResult").
			put("testMethodName", testClassName + "." + testMethodName);
		
		if (mutantWrapper!=null)
			response.put("mutantWrapper", mutantWrapper).
				put("testClassName", testClassName);
		
		Optional<Project> optProject = this.projectDAO.findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			
			JSONObject mavenResponse = MavenCompilerAndRunner.runTestMethod(project, mavenPath, desiredPath, javaHome, testClassName, testMethodName, testMethodId, dado);
			add(response, mavenResponse);
		} else {
			response.put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found in remote host"));
		}
		return response.toString();
	}
	
	public String stopTests(String projectId, String packageName) throws Exception {
		JSONObject response = new JSONObject();
		response.put("type", "testsStopped").
			put("packageName", packageName);
		
		return response.toString();
	}

	private void add(JSONObject response, JSONObject hostResponse) {
		Iterator<String> keys = hostResponse.keys();
		String key;
		while (keys.hasNext()) {
			key = keys.next();
			response.put(key, hostResponse.get(key));
		}
	}

	public synchronized JSONObject downloadMutant(Project project, JSONObject mutantWrapper, Object dado) throws Exception {
		JSONObject response = new JSONObject();
		response.put("type", "mutantDownloaded");
		project.downloadMutant(mutantWrapper.getString("id"), dado);		
		response.put("returnCode", 0).
			put("outputLines", new JSONArray()).
			put("errorLines", new JSONArray());
		return response;
	}
	
	public ClassicMutantDAO getClassicMutantDAO() {
		return classicMutantDAO;
	}
	
	public ProjectFileDAO getProjectFileDAO() {
		return projectFileDAO;
	}

	public ProjectDAO getProjectDAO() {
		return projectDAO;
	}

}
