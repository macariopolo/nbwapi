package edu.uclm.esi.nbwapi.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.nbwapi.model.Project;


@Repository
public interface ProjectDAO extends CrudRepository<Project, String>{

}
