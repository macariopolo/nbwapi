package edu.uclm.esi.nbwapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanzadoraNbwAPI {
    public static void main( String[] args) throws IOException {
    	String defaultPort = "8710";

    	System.out.print("Listening port (enter for default: " + defaultPort + "): ");
    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	
    	String sPort = reader.readLine().trim();
    	if (sPort.length()==0)
    		sPort = defaultPort;
    	
    	SpringApplication app = new SpringApplication(LanzadoraNbwAPI.class);
    	app.setDefaultProperties(Collections.singletonMap("server.port", sPort.trim()));
    	
    	app.run(args);
    }
}
