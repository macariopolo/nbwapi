package edu.uclm.esi.nbwapi.so;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.nbwapi.model.Manager;

public class ProcessExecutor implements Runnable {

	private String directory;
	private String command;
	private IProcessExecutorListener listener;
	private String responseType;
	private JSONObject executionResult;
	private JSONArray resultFiles;
	private String desiredPath;
	private String javaHome;
	
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddhhmmss");
	
	public ProcessExecutor(String directory) {
		this.directory = directory.replace('\\', '/');
		if (!this.directory.endsWith("/"))
			this.directory = this.directory + "/";
	}
	
	public ProcessExecutor(String directory, String command) {
		this(directory);
		this.command = command;
	}
	
	public ProcessExecutor(String directory, String command, String desiredPath, String javaHome) {
		this(directory, command);
		this.desiredPath = desiredPath;
		this.javaHome = javaHome;
	}
	
	public void start() {
		Thread thread = new Thread(this);
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		String[] tokens = command.split(" ");
		ProcessBuilder pb=new ProcessBuilder(tokens);
		if (this.desiredPath!=null) {
			Map<String, String> env = pb.environment();
			env.put("PATH", this.desiredPath);
		}
		if (this.javaHome!=null) {
			Map<String, String> env = pb.environment();
			env.put("JAVA_HOME", this.javaHome);
		}
		
		File file = new File(directory);
		pb.directory(file);
		
		Date date = new Date();
		String fileName = dateFormatter.format(date);
		File outputFile = new File(Manager.get().getIo() + fileName + ".output.txt");
		File errorsFile = new File(Manager.get().getIo() + fileName + ".errors.txt");
		pb.redirectOutput(outputFile);
		pb.redirectError(errorsFile);

		JSONObject executionResult = new JSONObject();
		executionResult.put("type", this.responseType);
		executionResult.put("id", UUID.randomUUID().toString());
		executionResult.put("command", command);
		executionResult.put("directory", directory);
		executionResult.put("returnCode", 0);
		executionResult.put("outputLines", new JSONArray());
		executionResult.put("errorLines", new JSONArray());
		
		JSONArray resultFiles = new JSONArray();
		resultFiles.put(outputFile.getAbsolutePath());
		resultFiles.put(errorsFile.getAbsolutePath());
		this.resultFiles = resultFiles;
		
		Process process;
		int returnCode = 0;
		try {
			process = pb.start();
			returnCode=process.waitFor();
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult(executionResult, resultFiles);
			return;
		}
		
		JSONArray outputLines=new JSONArray();
		try(FileReader fr=new FileReader(outputFile)) {  
			try(BufferedReader br=new BufferedReader(fr)) {
				String line=null;
				while ((line=br.readLine())!=null)
					outputLines.put(line);
			}
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult(executionResult, resultFiles);
			return;
		} 

		JSONArray errorLines=new JSONArray();
		try(FileReader fr=new FileReader(errorsFile)) {
			try(BufferedReader br=new BufferedReader(fr)) {
				String line=null;
				while ((line=br.readLine())!=null)
					errorLines.put(line);
			}
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult(executionResult, resultFiles);
			return;
		}
		
		executionResult.put("returnCode", returnCode);
		executionResult.put("outputLines", outputLines);
		executionResult.put("errorLines", errorLines);
		sendResult(executionResult, resultFiles);
	}

	private void sendResult(JSONObject executionResult, JSONArray resultFiles) {
		this.executionResult = executionResult;
		this.resultFiles = resultFiles;
		if (this.listener!=null) {
			executionResult.put("type", this.responseType);
			executionResult.put("files", resultFiles);
			this.listener.onProcessExecuted(executionResult);
		}
	}
	
	public JSONObject getExecutionResult() {
		this.executionResult.put("files", this.resultFiles);
		return executionResult;
	}
}