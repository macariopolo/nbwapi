package edu.uclm.esi.nbwapi.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ProjectFile {
	@Id
	private String id;
	@Column
	private String fileName;
	@Column(name = "contents")
	@JsonIgnore
	@Lob
	private byte[] contents;
	@Column(columnDefinition = "LONGTEXT")
	@Lob
	@JsonIgnore
	private String compilationUnit;
	@JsonIgnore
	
	public ProjectFile() {
		this.id = UUID.randomUUID().toString();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@JsonIgnore
	public byte[] getContents() {
		return contents;
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonIgnore
	public String getCompilationUnit() {
		return compilationUnit;
	}
	
	public void setCompilationUnit(String compilationUnit) {
		this.compilationUnit = compilationUnit;
	}
}
