package edu.uclm.esi.nbwapi.so;

import org.json.JSONObject;

public interface IProcessExecutorListener {

	void onProcessExecuted(JSONObject executionResult);

}
