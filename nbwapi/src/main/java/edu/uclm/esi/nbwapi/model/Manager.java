package edu.uclm.esi.nbwapi.model;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import edu.uclm.esi.nbwapi.dao.ClassicMutantDAO;
import edu.uclm.esi.nbwapi.dao.ProjectConfigurationDAO;
import edu.uclm.esi.nbwapi.dao.ProjectDAO;
import edu.uclm.esi.nbwapi.dao.ProjectFileDAO;
import edu.uclm.esi.nbwapi.dao.RemoteHostDAO;

@Component
public class Manager {
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ProjectConfigurationDAO projectConfigurationDAO;
	@Autowired
	private ProjectFileDAO projectFileDAO;
	@Autowired
	private ClassicMutantDAO classicMutantDAO;
	@Autowired
	private RemoteHostDAO remoteHostDAO;
	
	private String root;
	private String io;
	private String receivedAPKs;
	private String sdkPath;

	private Manager() {
		createFolders();
	}
	
	private void createFolders() {
		String homeFolder=System.getProperty("user.home");
		this.root = homeFolder + File.separatorChar + "nbwapi" + File.separatorChar;
		this.io = this.root + File.separatorChar + "io" + File.separatorChar;
		this.receivedAPKs = this.root + "receivedAPKs" + File.separatorChar;
		new File(this.root).mkdir();
		new File(this.io).mkdir();
		new File(this.receivedAPKs).mkdir();
	}
	
	public String getReceivedAPKs() {
		return this.receivedAPKs;
	}
	
	public ClassicMutantDAO getClassicMutantDAO() {
		return classicMutantDAO;
	}
	
	public String getIo() {
		return io;
	}
	
	public String getSdkPath() {
		return sdkPath;
	}
	
	public ProjectConfigurationDAO getProjectConfigurationDAO() {
		return projectConfigurationDAO;
	}
	
	public ProjectDAO getProjectDAO() {
		return projectDAO;
	}
	
	public ProjectFileDAO getProjectFileDAO() {
		return projectFileDAO;
	}
	
	public RemoteHostDAO getRemoteHostDAO() {
		return remoteHostDAO;
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	@Bean
	public static Manager get() {
		return ManagerHolder.singleton;
	}
}
