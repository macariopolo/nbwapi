package edu.uclm.esi.nbwapi.web.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIDevicesController {
	
	@PostMapping(value = "/launchEmulator", produces = MediaType.APPLICATION_JSON_VALUE)
	public void launchEmulator(HttpSession session, @RequestBody String emulator) throws Exception {
		System.out.println("launchEmulator " + emulator);
		AndroidAPIManager.launchEmulator(emulator);
	}
	
	@PostMapping(value = "/launchEmulators", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> launchEmulators(HttpSession session, @RequestBody List<String> emulators) throws Exception {
		System.out.print("launchEmulators " + new JSONArray(emulators).toString());
		return AndroidAPIManager.launchEmulators(emulators);
	}
	
	@PostMapping(value = "/stopEmulators", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> stopEmulators(HttpSession session, @RequestBody List<String> emulators) throws Exception {
		System.out.print("stopEmulators " + new JSONArray(emulators).toString());
		return AndroidAPIManager.stopEmulators(emulators);
	}
}
