package edu.uclm.esi.nbwapi.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Project {
	@Id
	private String id;
	private String name;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "configuration_id", referencedColumnName = "id")
	protected ProjectConfiguration projectConfiguration;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setProjectConfiguration(ProjectConfiguration projectConfiguration) {
		this.projectConfiguration = projectConfiguration;
	}
	
	public ProjectConfiguration getProjectConfiguration() {
		return projectConfiguration;
	}

	@JsonIgnore
	public String getProjectPath(Object dado) {
		return this.projectConfiguration.getProjectPath(this, dado);
	}

	public synchronized void downloadMutant(String mutantId, Object dado) throws Exception {
		this.projectConfiguration.downloadMutant(this, mutantId, dado);
	}
	
	public synchronized void downloadOriginalFile(String projectFileId, Object dado) throws Exception {
		this.projectConfiguration.downloadOriginalFile(this, projectFileId, dado);
	}
}
