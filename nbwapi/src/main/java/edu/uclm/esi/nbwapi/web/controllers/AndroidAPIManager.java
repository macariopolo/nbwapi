package edu.uclm.esi.nbwapi.web.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.nbwapi.model.APKCompiler;
import edu.uclm.esi.nbwapi.model.AndroidProjectConfiguration;
import edu.uclm.esi.nbwapi.model.Device;
import edu.uclm.esi.nbwapi.model.Manager;
import edu.uclm.esi.nbwapi.model.Project;
import edu.uclm.esi.nbwapi.model.ProjectFile;
import edu.uclm.esi.nbwapi.model.RemoteHost;
import edu.uclm.esi.nbwapi.so.ProcessExecutor;

public class AndroidAPIManager {
	private static List<Device> devices;
	
	private static RemoteHost host;
	
	private static RemoteHost getHost() throws UnknownHostException {
		if (host!=null)
			return host;
		String ip = InetAddress.getLocalHost().getHostAddress();		
		Optional<RemoteHost> optHost = Manager.get().getRemoteHostDAO().findById(ip);
		if (optHost.isPresent())
			return optHost.get();
		throw new UnknownHostException();
	}
	
	public static List<String> loadEmulators() throws Exception {
		List<String> emulators = getHost().loadEmulators();
		return emulators;
	}

	public static List<Device> loadDevices() throws Exception {
		devices = new Vector<>();
		devices = getHost().loadDevices();
		return devices;
	}
	
	public static void launchEmulator(String emulator) throws Exception {
		getHost().launchEmulator(emulator);
	}

	public static List<String> launchEmulators(List<String> emulators) throws Exception {
		return getHost().launchEmulators(emulators);
	}

	public static List<String> stopEmulators(List<String> emulators) throws Exception {
		return getHost().stopEmulators(emulators);
	}

	public static String downloadProject(String projectId, Object dado) throws FileNotFoundException, IOException {
		JSONObject response = new JSONObject();
		response.put("type", "projectDownloaded");

		String path = Manager.get().getReceivedAPKs() + dado + File.separatorChar;
		try { FileUtils.forceDelete(new File(path)); } catch (Exception e) { }

		Project project = Manager.get().getProjectDAO().findById(projectId).get();
		path = path + project.getName() + '/';
		File file = new File(path);
		file.mkdirs();

		List<String> files = Manager.get().getProjectFileDAO().findIdsByProject(projectId);
		String projectFileId;
		ProjectFile projectFile;
		String filePath;
		int posLastBarra;
		int numberOfFiles = files.size();
		int contador = 0;
		while (files.size()>0) {
			contador++;
			if (contador % 100 ==0) {
				System.out.println(contador + "/" + numberOfFiles);
			}
			projectFileId = files.remove(0);
			try {
				projectFile = Manager.get().getProjectFileDAO().findById(projectFileId).get();
				filePath = projectFile.getFileName();
				posLastBarra = filePath.lastIndexOf('/');
				if (posLastBarra!=-1) {
					filePath = path + filePath.substring(0, filePath.lastIndexOf('/'));
					new File(filePath).mkdirs();
				}
				filePath = path + projectFile.getFileName();
				try(FileOutputStream fos = new FileOutputStream(filePath)) {
					fos.write(projectFile.getContents());
				}
			} catch (Error e) {
				e.printStackTrace();
			}
		}
		
		try {
			updateLocalPropertiesFile(projectId);
			response.put("returnCode", 0);
		} catch (Exception e) {
			response.put("returnCode", -1).
				put("errorLines", new JSONArray().put(e.getMessage())).
				put("outputLines", new JSONArray());
		}
		return response.toString();
	}
	
	private static void updateLocalPropertiesFile(String projectId) throws Exception {
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			String localPropertiesPath= Manager.get().getReceivedAPKs() + 
					project.getId() + File.separatorChar + 
					project.getName() + File.separatorChar +
					((AndroidProjectConfiguration) project.getProjectConfiguration()).getLocalProperties();
			
			try(FileReader fr=new FileReader(localPropertiesPath)) {
				BufferedReader br=new BufferedReader(fr);
				String line=null;
				Vector<String> lines=new Vector<>();
				while ((line=br.readLine())!=null && !line.startsWith("sdk.dir")) 
					lines.add(line);
				line="sdk.dir=" + Manager.get().getSdkPath();
				lines.add(line);
				PrintWriter pw=new PrintWriter(localPropertiesPath);
				for (String sLine : lines)
					pw.println(sLine);
				pw.close();
			}
		}	
	}

	public static JSONObject compile(String projectId, String mutantId, Object dado) {
		JSONObject response = new JSONObject();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			try {
				response = APKCompiler.compile(project, mutantId, dado);
			} catch (Exception e) {
				response.put("returnCode", -1);
			}
			response.put("type", "originalAPKCompiled");
		} else {
			response.put("type", "originalAPKCompiled").put("returnCode", -1).
			put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response;
	}
	
	public static String compileTest(String projectId, Object dado) {
		JSONObject response = new JSONObject();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			try {
				response = APKCompiler.compileTest(project, dado);
				response.put("type", "testAPKCompiled");
			} catch (Exception e) {
				response.put("returnCode", -1);
			}
		} else {
			response.put("type", "testAPKCompiled").put("returnCode", -1).
			put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response.toString();
	}

	public static JSONObject push(String deviceName, String projectId, boolean testAPK, Object dado) throws Exception {
		JSONObject response = new JSONObject();
		response.put("deviceName", deviceName);
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			Device device = findDevice(deviceName);
		
			JSONObject deviceResponse = device.push(project, testAPK, dado);
			add(response, deviceResponse);
		} else {
			response.put("type", testAPK ? "testAPKPushed" : "APKPushed").
			put("returnCode", -1).
			put("outputLines", new JSONArray()).
			put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response;
	}
	
	public static JSONObject install(String deviceName, String projectId, boolean testAPK, Object dado) throws Exception {
		JSONObject response = new JSONObject();
		response.put("deviceName", deviceName);
		
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			Device device = findDevice(deviceName);
			
			JSONObject deviceResponse = device.install(project, testAPK, dado);
			add(response, deviceResponse);
		} else {
			response.put("type", testAPK ? "testAPKInstalled" : "APKInstalled").
				put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response;
	}
	
	public static String runTestMethod(String projectId, String deviceName, String testClassName, String testMethodName, JSONObject mutantWrapper) throws Exception {
		JSONObject response = new JSONObject();
		response.put("deviceName", deviceName).
			put("type", "testResult").
			put("testMethodName", testMethodName);
		
		if (mutantWrapper!=null)
			response.put("mutantWrapper", mutantWrapper).
				put("testClassName", testClassName);
		
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			Device device = findDevice(deviceName);
			
			JSONObject deviceResponse = device.runTestMethod(Manager.get().getSdkPath(), testClassName, testMethodName,
					((AndroidProjectConfiguration) project.getProjectConfiguration()).getTestRunner());
			add(response, deviceResponse);
		} else {
			response.put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response.toString();
	}
	
	public static String stopTests(String projectId, String deviceName, String packageName) throws Exception {
		JSONObject response = new JSONObject();
		response.put("deviceName", deviceName).
			put("type", "testsStopped").
			put("packageName", packageName);
		
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			// am force-stop com.maco.figures
			String command = Manager.get().getSdkPath() + "platform-tools/adb -s " + deviceName + 
					" shell am force-stop " + ((AndroidProjectConfiguration) project.getProjectConfiguration()).getDebugPackage();
			
			ProcessExecutor processExecutor = new ProcessExecutor(Manager.get().getIo(), command);
			processExecutor.start();
			JSONObject deviceResponse = processExecutor.getExecutionResult();
			add(response, deviceResponse);
		} else {
			response.put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response.toString();
	}

	private static void add(JSONObject response, JSONObject deviceResponse) {
		Iterator<String> keys = deviceResponse.keys();
		String key;
		while (keys.hasNext()) {
			key = keys.next();
			response.put(key, deviceResponse.get(key));
		}
	}

	private static Device findDevice(String deviceName) throws Exception {
		if (devices==null || devices.isEmpty())
			loadDevices();
		for (Device device : devices)
			if (device.getName().equals(deviceName))
				return device;
		throw new Exception("Device " + deviceName + " not found");
	}

	public static JSONObject downloadMutant(String projectId, JSONObject mutantWrapper, Object dado) throws Exception {
		JSONObject response = new JSONObject();
		response.put("type", "mutantDownloaded");
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			project.downloadMutant(mutantWrapper.getString("id"), dado);		
			response.put("returnCode", 0).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray());
		} else {
			response.put("returnCode", -1).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put("Project not found by remote device"));
		}
		return response;
	}
}
